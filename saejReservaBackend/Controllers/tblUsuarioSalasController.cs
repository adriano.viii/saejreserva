﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saejReservaBackend;

namespace saejReservaBackend.Controllers
{
    public class tblUsuarioSalasController : Controller
    {
        private saejReservaDBEntities db = new saejReservaDBEntities();

        // GET: tblUsuarioSalas
        public ActionResult Index()
        {
            var tblUsuarioSala = db.tblUsuarioSala.Include(t => t.tblSala).Include(t => t.tblUsuario);
            return View(tblUsuarioSala.ToList());
        }

        // GET: tblUsuarioSalas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUsuarioSala tblUsuarioSala = db.tblUsuarioSala.Find(id);
            if (tblUsuarioSala == null)
            {
                return HttpNotFound();
            }
            return View(tblUsuarioSala);
        }

        // GET: tblUsuarioSalas/Create
        public ActionResult Create()
        {
            ViewBag.SalaID = new SelectList(db.tblSala, "Id", "SalaTipoID");
            ViewBag.UsuarioID = new SelectList(db.tblUsuario, "id", "UsuarioTipoId");
            return View();
        }

        // POST: tblUsuarioSalas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UsuarioID,SalaID,UsuarioSalaFecha")] tblUsuarioSala tblUsuarioSala)
        {
            if (ModelState.IsValid)
            {
                db.tblUsuarioSala.Add(tblUsuarioSala);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SalaID = new SelectList(db.tblSala, "Id", "SalaTipoID", tblUsuarioSala.SalaID);
            ViewBag.UsuarioID = new SelectList(db.tblUsuario, "id", "UsuarioTipoId", tblUsuarioSala.UsuarioID);
            return View(tblUsuarioSala);
        }

        // GET: tblUsuarioSalas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUsuarioSala tblUsuarioSala = db.tblUsuarioSala.Find(id);
            if (tblUsuarioSala == null)
            {
                return HttpNotFound();
            }
            ViewBag.SalaID = new SelectList(db.tblSala, "Id", "SalaTipoID", tblUsuarioSala.SalaID);
            ViewBag.UsuarioID = new SelectList(db.tblUsuario, "id", "UsuarioTipoId", tblUsuarioSala.UsuarioID);
            return View(tblUsuarioSala);
        }

        // POST: tblUsuarioSalas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UsuarioID,SalaID,UsuarioSalaFecha")] tblUsuarioSala tblUsuarioSala)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblUsuarioSala).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SalaID = new SelectList(db.tblSala, "Id", "SalaTipoID", tblUsuarioSala.SalaID);
            ViewBag.UsuarioID = new SelectList(db.tblUsuario, "id", "UsuarioTipoId", tblUsuarioSala.UsuarioID);
            return View(tblUsuarioSala);
        }

        // GET: tblUsuarioSalas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUsuarioSala tblUsuarioSala = db.tblUsuarioSala.Find(id);
            if (tblUsuarioSala == null)
            {
                return HttpNotFound();
            }
            return View(tblUsuarioSala);
        }

        // POST: tblUsuarioSalas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            tblUsuarioSala tblUsuarioSala = db.tblUsuarioSala.Find(id);
            db.tblUsuarioSala.Remove(tblUsuarioSala);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
