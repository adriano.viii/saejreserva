﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace saejReservaBackend.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Reserva y asignación de salones de SAEJ.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Información de Contacto.";

            return View();
        }
    }
}