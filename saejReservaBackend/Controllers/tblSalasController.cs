﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saejReservaBackend;

namespace saejReservaBackend.Controllers
{
    public class tblSalasController : Controller
    {
        private saejReservaDBEntities db = new saejReservaDBEntities();

        // GET: tblSalas
        public ActionResult Index()
        {
            return View(db.tblSala.ToList());
        }

        // GET: tblSalas/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSala tblSala = db.tblSala.Find(id);
            if (tblSala == null)
            {
                return HttpNotFound();
            }
            return View(tblSala);
        }

        // GET: tblSalas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblSalas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SalaTipoID,SalaEstado,SalaNombre,SalaDescripcion")] tblSala tblSala)
        {
            if (ModelState.IsValid)
            {
                db.tblSala.Add(tblSala);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblSala);
        }

        // GET: tblSalas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSala tblSala = db.tblSala.Find(id);
            if (tblSala == null)
            {
                return HttpNotFound();
            }
            return View(tblSala);
        }

        // POST: tblSalas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SalaTipoID,SalaEstado,SalaNombre,SalaDescripcion")] tblSala tblSala)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblSala).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblSala);
        }

        // GET: tblSalas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSala tblSala = db.tblSala.Find(id);
            if (tblSala == null)
            {
                return HttpNotFound();
            }
            return View(tblSala);
        }

        // POST: tblSalas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            tblSala tblSala = db.tblSala.Find(id);
            db.tblSala.Remove(tblSala);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
