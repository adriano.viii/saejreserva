﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(saejReservaBackend.Startup))]
namespace saejReservaBackend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
