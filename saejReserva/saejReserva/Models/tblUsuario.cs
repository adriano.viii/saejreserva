﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saejReserva.Models
{
    class tblUsuario
    {

        public string Id { get; set; }
        public string UsuarioTipoID { get; set; }
        public string UsuarioNombre { get; set; }
        public string UsuarioApellido { get; set; }
        public string UsuarioCodigo { get; set; }
        public string UsuarioEmail { get; set; }
        public string UsuarioPassword { get; set; }
    }
}
