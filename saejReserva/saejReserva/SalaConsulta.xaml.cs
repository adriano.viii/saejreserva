﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace saejReserva
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SalaConsulta : ContentPage
    {
        public SalaConsulta()
        {
            InitializeComponent();
        }

        private void cmdConsultar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new UsuarioSalaResultado());
        }
    }
}