﻿using saejReserva.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace saejReserva
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            bool isEmailEmpty = string.IsNullOrEmpty(emailEntry.Text);
            bool isPasswordEmpty = string.IsNullOrEmpty(passwordEntry.Text);

            if (isEmailEmpty || isPasswordEmpty)
            {
                await DisplayAlert("Error", "Datos incompletos", "Ok");
            }
            else
            {
                var usuario = (await App.mobileService.GetTable<tblUsuario>().Where(u => u.UsuarioEmail == emailEntry.Text).ToListAsync()).FirstOrDefault();
                if (usuario != null)
                {
                    if (usuario.UsuarioPassword == passwordEntry.Text)
                        await Navigation.PushAsync(new HomePage());
                    else
                        await DisplayAlert("Error", "Email o password incorrecto", "Ok");
                }
                else
                {
                    await DisplayAlert("Error", "Email o password incorrecto", "Ok");
                }
            }
        }

        private void RegisterUserButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegisterPage());
        }
    }
}
