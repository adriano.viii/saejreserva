﻿using saejReserva.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace saejReserva
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            if (passwordEntry.Text == confirmPasswordEntry.Text)
            {
                try
                {
                    tblUsuario NewUser = new tblUsuario()
                    {
                        UsuarioNombre = nombreEntry.Text,
                        UsuarioApellido = apellidosEntry.Text,
                        UsuarioCodigo = codigoEntry.Text,
                        UsuarioEmail = emailEntry.Text,
                        UsuarioPassword = passwordEntry.Text
                    };
                    await App.mobileService.GetTable<tblUsuario>().InsertAsync(NewUser);
                    await Navigation.PushAsync(new HomePage());
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Error", ex.Message, "Ok");
                }
            }
            else
            {
                await DisplayAlert("Error", "Passwords diferentes", "Ok");
            }
        }

    }
}